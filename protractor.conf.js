'use strict';

// An example configuration file.
exports.config = {
  // The address of a running selenium server.
  //seleniumAddress: 'http://localhost:4444/wd/hub',
  //seleniumAddress:'http://hub.browserstack.com/wd/hub',
  //seleniumServerJar: deprecated, this should be set on node_modules/protractor/config.json

  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  restartBrowserBetweenTests: true,
  ignoreUncaughtExceptions:true,
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: ['disable-popup-blocking']
    }
  },
  specs: 'e2e/patientportal/features/**/*.feature',
  getPageTimeout: 60000,
  allScriptsTimeout: 90000,
  resultJsonOutputFile: '.tmp/e2e/patientportal/protractor-test-results.json',
  cucumberOpts: {
    require: ['support/*.js', '.tmp/e2e/patientportal/**/*.js'],
    format: 'pretty',
    tags: '@Completed'
  }
};

