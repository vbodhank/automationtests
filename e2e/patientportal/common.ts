/// <reference path="../../typings/index.d.ts" />

interface e2eConfig {
  url : string;
  patientUserName: string;
  patientPassword: string;
  patientWithoutDataUsername: string;
  patientWithoutDatapassword: string;
  timeout: number;
}

interface Function {
  Given(s:RegExp, f:(...o) => void):void;
  Then(s:RegExp, f:(...o) => void):void;
  When(s:RegExp, f:(...o) => void):void;
  And(s:RegExp, f:(...o) => void):void;
  And(s:RegExp, f:(...o) => void):void;
  After(f:(...o) => void):void;
  isPresent(e:protractor.ElementFinder):webdriver.promise.Promise<any>;
  isAbsent(e:protractor.ElementFinder):webdriver.promise.Promise<any>;
  logOut():webdriver.promise.Promise<any>;
  config: e2eConfig;
  expect :any
}

var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
var expect = chai.expect;
var request = require('request');
var argv = require('yargs').argv;

function World() {

  this.newBrowserWindow = null;
  this.config = {
    browserWidth: 1920,
    browserHeight: 1080,
    url: 'http://localhost:3000',
    patientUserName: 'patientwithdata',
    patientPassword: 'G0T1@2016',
    patientWithoutDataUsername: 'jsmith',
    patientWithoutDatapassword: 'H@lo2015',
    oncologistUserName: 'oncologist',
    delegateWithViewRightsUsername: 'wburkl',
    delegateWithViewRightsPassword: 'H@lo2015',
    delegateWithAllRightsUsername: 'dburkl',
    delegateWithAllRightsPassword: 'H@lo2015',
    delegateWithoutRightsUsername: 'rburkl',
    delegateWithoutRightsPassword: 'H@lo2015',
    helpUserName: 'resetPassword',
    helpUserPassword: 'H@lo2015',

    timeout: 60 * 1000,
    oncologyClientUrl: 'http://localhost:8000/build/app.html#',
    oncologyServerUrl: 'http://localhost:8001',
    oncologist: {
      'firstName': 'Fe2etestuser',
      'lastName': 'Le2etestuser',
      'username': 'oncologist@hdev.in',
      'password': 'Onc0l0g!st'
    },
    clinicalContact: {
      'firstName': 'Fe2etestuser',
      'lastName': 'Le2etestuser',
      'email': 'vmshalo@gmail.com',
      'number': '(817)635-4450'
    },
    vmsadmin: {'username': 'vmsadmin@hdev.in', 'password': 'V@rian02'},
    clerk: {'username': 'clerk@hdev.in', 'password': 'C1erk@l23'},
    nurse: {'username': 'nurse@hdev.in', 'password': 'Nurs311!'},
    resetPassword: {'username': 'resetpassword', 'password': 'As1@asd22'},
    useAzureAD: 'replaceAzureADFromTag',
    isPatientPortal : true,
    testEnv: 'replaceTestEnvFromParam'
  };

  this.loginIntoPortal = (username, password, next) => {
    this.isPatientPortal = true;
    if (this.config.useAzureAD === 'true') {
      browser.sleep(3000);

      browser.driver.isElementPresent(By.id('SignInWithLogonNameExchange')).then((exist)=>{
        if (exist){
          browser.driver.findElement(By.id('SignInWithLogonNameExchange')).click();
        }
      });

      browser.driver.isElementPresent(By.id('cred_userid_inputtext')).then((result)=> {
        if (result) {
          loginIntoAD(username, password, this, next);
        } else {
          removeToast(this, next);
        }
      });
    } else {
      browser.waitForAngular();
      element(by.model('username')).isPresent().then((result)=> {
        if (result) {
          element(by.model('username')).sendKeys(username);
          element(by.model('password')).sendKeys(password);
          element(by.css('button[type="submit"]')).click()
            .then(next);
        } else {
          next();
        }
      });
    }
  };

  this.isPresent = (e:protractor.ElementFinder) => {
    return e.isPresent().then((result) => expect(result).to.equal(true));
  };

  this.isDisabled = (e:protractor.ElementFinder) => {
    return e.isEnabled().then((result) => expect(result).to.equal(false));
  };

  this.isEnabled = (e:protractor.ElementFinder) => {
    return e.isEnabled().then((result) => expect(result).to.equal(true));
  };

  this.logOut = () => {
    return element(by.css('#signout')).click();
  };

  this.isAbsent = (e:protractor.ElementFinder) => {
    return e.isPresent().then((result) => expect(result).to.equal(false));
  };


  this.selectDropdownItem = (selector, item) => {

    var deferred = protractor.promise.defer();

    selector.click().then(
      ()=> {
        var EC = protractor.ExpectedConditions;
        var dropDownPopup = element(by.css('div.md-select-menu-container[aria-hidden=false]'));

        browser.wait(EC.presenceOf(dropDownPopup), 5000).then(()=>{

            dropDownPopup.element(by.cssContainingText('md-option', item)).click().then(()=> {
              deferred.fulfill();
            });
        },()=>{
          element(by.cssContainingText('md-option', item)).click().then(()=> {
            deferred.fulfill();
          });
        });

      }
    );

    return deferred.promise;
  };


  this.selectDropdownItemByIndex = (selector, index) => {

    var deferred = protractor.promise.defer();

    selector.click().then(
      ()=> {
        var EC = protractor.ExpectedConditions;
        var dropDownPopup = element(by.css('div.md-select-menu-container[aria-hidden=false]'));

        browser.wait(EC.presenceOf(dropDownPopup), 5000).then(()=>{
          dropDownPopup.all(by.tagName('md-option')).get(index).click().then(()=> {
            deferred.fulfill();
          });
        },()=>{
          element.all(by.tagName('md-option')).get(index).click().then(()=> {
            deferred.fulfill();
          });
        });

      }
    );

    return deferred.promise;
  };

  this.selectDropdownItemSelectAll = (selector, item) => {

    var deferred = protractor.promise.defer();

    selector.click().then(
      ()=> {
        var EC = protractor.ExpectedConditions;
        var dropDownPopup = element(by.css('div.md-select-menu-container[aria-hidden=false]'));

        browser.wait(EC.presenceOf(dropDownPopup), 5000).then(()=>{
          dropDownPopup.element(by.cssContainingText('md-optgroup', item)).click().then(()=> {
            deferred.fulfill();
          });
        },()=>{
          element(by.cssContainingText('md-optgroup', item)).click().then(()=> {
            deferred.fulfill();
          });
        });

      }
    );

    return deferred.promise;
  };


  this.closeSecurityWarning = (currentBrowser:protractor.IBrowser, next:any)=> {
    this.isPatientPortal = false;
    currentBrowser.sleep(10000);
    var elementLocator = by.css('[ng-click="closeToast()"]');
    currentBrowser.driver.isElementPresent(elementLocator).then((result)=> {
      if (result) {
        currentBrowser.driver.executeScript("arguments[0].click()", currentBrowser.driver.findElement(elementLocator)).then(next);
      } else {
        next();
      }
    });
  };

  this.logOutFromClient = (currentBrowser:protractor.IBrowser, next:any)=> {
    currentBrowser.waitForAngular();
    currentBrowser.element(by.css('[ng-click="showUserInformation=!showUserInformation;"]')).click().then(function () {
      currentBrowser.waitForAngular();
      currentBrowser.element(by.className('log-out')).click().then(()=> {
        currentBrowser.sleep(10000);
        next();
      });
    });
  };

  this.setupDB = function () {

    var deferred = protractor.promise.defer();

    var options = {
      method: 'POST',
      url: this.config.oncologyServerUrl + '/setupTestData',
      headers: {
        // 'Authorization':'Basicdm1zYWRtaW46dm1zYWRtaW4=',
        'Content-Type': 'application/json'
      },
      json: {
        username: 'jsmith',
        password: 'jsmith'
      }
    };

    function callback(error, response, body) {
      console.log("insidecallback");
      deferred.fulfill();
      /*if(!error){
       console.log("insidecallbackdeferredfulfill");
       var info=JSON.parse(body);
       console.log(response);
       console.log(info);
       deferred.fulfill();
       }*/
    }

    request(options, callback);
    return deferred.promise;
  };

  this.approvePRO = function () {

    var deferred = protractor.promise.defer();
    var serverUrl = this.config.oncologyServerUrl;

    this.getAccessTokenFromAD().then(onTokenReceived);

    function onTokenReceived(token) {

      var options = {
        method: 'POST',
        url: serverUrl + '/approvePRO',
        auth: {'bearer': token},
        headers: {
          'Content-Type': 'application/json'
        }
      };

      request(options, callback);
    }

    function callback(error, response, body) {
      console.log("\ninside oncology server callback");
      deferred.fulfill();
    }


    return deferred.promise;
  };

    this.setPRDApprovalRight = function () {

      var deferred = protractor.promise.defer();
      var serverUrl = this.config.oncologyServerUrl;

      this.getAccessTokenFromAD().then(onTokenReceived);

      function onTokenReceived(token) {

        var options = {
          method: 'POST',
          url: serverUrl + '/setPRDApprovalRight',
          auth: {'bearer': token},
          headers: {
            'Content-Type': 'application/json'
          }
        };

        request(options, callback);
      }

      function callback(error, response, body) {
        console.log("\ninside oncology server callback");
        deferred.fulfill();
      }


      return deferred.promise;
    };

  this.resetPRDApprovalRight = function () {

    var deferred = protractor.promise.defer();
    var serverUrl = this.config.oncologyServerUrl;

    this.getAccessTokenFromAD().then(onTokenReceived);

    function onTokenReceived(token) {

      var options = {
        method: 'POST',
        url: serverUrl + '/resetPRDApprovalRight',
        auth: {'bearer': token},
        headers: {
          'Content-Type': 'application/json'
        }
      };

      request(options, callback);
    }

    function callback(error, response, body) {
      console.log("\ninside oncology server callback");
      deferred.fulfill();
    }


    return deferred.promise;
  };

  this.rejectPRO = function () {

    var deferred = protractor.promise.defer();
    var serverUrl = this.config.oncologyServerUrl;

    this.getAccessTokenFromAD().then(onTokenReceived);

    function onTokenReceived(token) {

      var options = {
        method: 'POST',
        url: serverUrl + '/rejectPRO',
        auth: {'bearer': token},
        headers: {
          'Content-Type': 'application/json'
        }
      };

      request(options, callback);
    }

    function callback(error, response, body) {
      console.log("insidecallback");
      deferred.fulfill();
    }

    return deferred.promise;
  };

  this.approveAllergy = function () {

    var deferred = protractor.promise.defer();
    var serverUrl = this.config.oncologyServerUrl;

    this.getAccessTokenFromAD().then(onTokenReceived);

    function onTokenReceived(token) {

      var options = {
        method: 'POST',
        url: serverUrl + '/approveAllergy',
        auth: {'bearer': token},
        headers: {
          'Content-Type': 'application/json'
        }
      };

      request(options, callback);
    }

    function callback(error, response, body) {
      console.log("insidecallback");
      deferred.fulfill();
    }

    return deferred.promise;
  };

  this.rejectAllergy = function () {

    var deferred = protractor.promise.defer();
    var serverUrl = this.config.oncologyServerUrl;

    this.getAccessTokenFromAD().then(onTokenReceived);

    function onTokenReceived(token) {

      var options = {
        method: 'POST',
        url: serverUrl + '/rejectAllergy',
        auth: {'bearer': token},
        headers: {
          'Content-Type': 'application/json'
        }
      };

      request(options, callback);
    }

    function callback(error, response, body) {
      console.log("insidecallback");
      deferred.fulfill();
    }

    return deferred.promise;
  };

  this.approveInsurance = function () {

    var deferred = protractor.promise.defer();
    var serverUrl = this.config.oncologyServerUrl;

    this.getAccessTokenFromAD().then(onTokenReceived);

    function onTokenReceived(token) {

      var options = {
        method: 'POST',
        url: serverUrl + '/approveInsurance',
        auth: {'bearer': token},
        headers: {
          'Content-Type': 'application/json'
        }
      };

      request(options, callback);
    }

    function callback(error, response, body) {
      console.log("insidecallback");
      deferred.fulfill();
    }

    return deferred.promise;
  };

  this.rejectInsurance = function () {

    var deferred = protractor.promise.defer();
    var serverUrl = this.config.oncologyServerUrl;

    this.getAccessTokenFromAD().then(onTokenReceived);

    function onTokenReceived(token) {

      var options = {
        method: 'POST',
        url: serverUrl + '/rejectInsurance',
        auth: {'bearer': token},
        headers: {
          'Content-Type': 'application/json'
        }
      };

      request(options, callback);
    }

    function callback(error, response, body) {
      console.log("insidecallback");
      deferred.fulfill();
    }

    return deferred.promise;
  };

  this.createAppointment = function () {

    var deferred = protractor.promise.defer();
    var serverUrl = this.config.oncologyServerUrl;

    this.getAccessTokenFromAD().then(onTokenReceived);

    function onTokenReceived(token) {
      var options = {
        method: 'POST',
        url: serverUrl + '/createAppointment',
        auth: {'bearer': token},
        headers: {
          'Content-Type': 'application/json'
        }
      };

      request(options, callback);
    }

    function callback(error, response, body) {
      console.log("insidecallback");
      deferred.fulfill();
    }

    return deferred.promise;
  };

  this.getAccessTokenFromAD = function () {

    var deferred = protractor.promise.defer();

    // Request Access Token from AD
    request({
      url: 'https://login.microsoftonline.com/hdev.in/oauth2/token',
      method: 'POST',
      timeout: 1200000,
      form: {
        grant_type: 'password',
        client_id: '04b07795-8ddb-461a-bbee-02f9e1bf7b46',
        username: 'e2euser@hdev.in',
        password: 'H@lo2015',
        resource: 'https://management.core.windows.net/'
      }
    }, function (error, response, body) {

      if (response === undefined) {
        console.log('AD Server seems to be down');
        deferred.reject('AD Server seems to be down');
      } else if (!error && (response.statusCode == 200)) {
        console.log("Access Token Received");
        var body = JSON.parse(response.body);
        deferred.fulfill(body.access_token);
      } else {
        console.log(response);
        console.log('Error occurred while getting access token from AD :' + response.statusCode + " " + response.statusMessage);
        deferred.reject('Error occurred while getting access token from AD :' + response.statusCode + " " + response.statusMessage);
      }
    });

    return deferred.promise;
  }

  this.expect = expect;

  function loginIntoAD(username, password, scope, next) {
    browser.executeScript("document.getElementById('cred_userid_inputtext').value='" + username + "'");
    browser.executeScript("document.getElementById('cred_password_inputtext').value='" + password + "'");
    browser.executeScript("document.getElementById('credentials').submit()");
    removeToast(scope, next);
  }

  function removeToast(scope, next) {
    var EC = protractor.ExpectedConditions;
    var waitTime = scope.config.testEnv === 'desktop' ? 60000 : 20000;
    var warn = element(by.css('.toast-warning'));
    browser.ignoreSynchronization = true;
    browser.wait(EC.presenceOf(warn), waitTime).then(() => {
      browser.ignoreSynchronization = false;
      var toasterCloseBtn = warn.element(by.css('.toast-close-button'));
      toasterCloseBtn.click().then(()=> {
        browser.wait(EC.stalenessOf(warn), 5000).then(() => {
          browser.executeScript("window.shouldShowWarning = 'N'").then(()=> {
            next();
          });
        });
      });
    }, ()=> {
      console.log('--toastr not found---');
      browser.ignoreSynchronization = false;
      next();
    });
  }


}

module.exports.World = World;


