/// <reference path="../common.ts" />
var steps = function() {

    var c:any = require('../common');
    this.World = c.World;

    this.Given(/^I navigate to PatientPortalUrl$/, function (next) {
        // Write code here that turns the phrase above into concrete actions
        browser.manage().window().setSize(this.config.browserWidth, this.config.browserHeight)
            .then(() => {
                browser.get(this.config.url).then(()=> {
                    next();
                }, ()=> {
                    next();
                });
            });
    });

    this.When(/^I log in using valid credentials$/, function (next) {
        this.loginIntoPortal(this.config.patientUserName, this.config.patientPassword, next);
    });


    this.Then(/^I should be able to see the Hub page$/, function (next) {
        this.isPresent(element(by.css('#patient-profile'))).then(() => next());
    });

    this.After(function (scenario, next) {
        var self = this;
        if (scenario.isFailed()) {
            if (this.isPatientPortal) {
                browser.takeScreenshot().then(function (png) {
                    var decodedImage = new Buffer(png, 'base64').toString('binary');
                    scenario.attach(decodedImage, 'image/png');
                });
            } else {
                this.newBrowserWindow.takeScreenshot().then(function (png) {
                    var decodedImage = new Buffer(png, 'base64').toString('binary');
                    scenario.attach(decodedImage, 'image/png');
                });
            }
        }
        if (this.newBrowserWindow) {
            self.newBrowserWindow.quit();
            self.newBrowserWindow = null;
        }
        browser.executeScript("window.localStorage.clear();");
        browser.executeScript("window.sessionStorage.clear();").then(next);
    });

    function clearCache(){
        browser.executeScript("window.localStorage.clear();");
        browser.executeScript("window.sessionStorage.clear();");
        browser.manage().deleteAllCookies();
        browser.executeScript("window.location.reload(true);");
    }
}

module.exports = steps;