@Completed
Feature: PatientPortal  Login

  Log in, terms & conditions and UI tour functionality for Patient and delegate

  @PP-91
  Scenario: I should be able to login when using valid credentials

    Given I navigate to PatientPortalUrl
    When I log in using valid credentials
    Then I should be able to see the Hub page