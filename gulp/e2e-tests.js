'use strict';

var gulp = require('gulp');

var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});
var gulpTypings = require("gulp-typings");
//var reporter = require('cucumber-html-reporter');
var reporter = require('gulp-protractor-cucumber-html-report');

var browserSync = require('browser-sync');
var argv = require('yargs').argv;
var replace = require('gulp-replace');
var fs = require('fs-extra');
var ts = require('gulp-typescript');
var protractor = require("gulp-protractor");

module.exports = function(options) {

    gulp.task('webdriver-update', protractor.webdriver_update);

    gulp.task('webdriver-standalone', protractor.webdriver_standalone);

    gulp.task("installTypings",function(){
        var stream = gulp.src("./typings.json")
            .pipe(gulpTypings()); //will install all typingsfiles in pipeline.
        return stream; // by returning stream gulp can listen to events from the stream and knows when it is finished.
    });

    gulp.task('transpile-e2e' , function(){
        return gulp.src('e2e/**/*.ts')
            .pipe(replace('http://localhost:3000', argv.e2eurl ? argv.e2eurl : 'http://localhost:3000'))
            .pipe(replace('replaceAzureADFromTag', argv.useAzureAD ? argv.useAzureAD : false))
            .pipe(
                ts({
                    "compilerOptions": {
                        "target": "es5",
                        "sourceMap": true,
                        "allowJs": false
                    },
                    "exclude": [
                        "./node_modules/**/*.ts",
                        "./node_modules/**/*.js"
                    ]
                })
            )
            .pipe(gulp.dest(options.tmp + '/e2e/'));
    });

    function runProtractor(done) {

        var args = [];
        var configFile = 'protractor.conf.js';

        gulp.src('e2e/patientportal/features/**/*.feature')
            .pipe(protractor.protractor({
                configFile: configFile,
                args: args
            }))
            .on('error', function (err) {
                // Make sure failed tests cause gulp to exit non-zero
                throw err;
            })
            .on('end', function () {
                // Close browser sync server
                browserSync.exit();
                done();
            });
    }

    gulp.task('clean', function (done) {
        $.del([options.tmp + '*', 'reports*'], done);
    });

    gulp.task('Generate-TestResult', function (done) {
        var resultFile = 'cucumber-test-results.json';
        var reportLocation = 'reports/';


        gulp.src(options.tmp + '/e2e/patientportal/' + resultFile)
            .pipe(replace('][', ','))
            .pipe(gulp.dest(options.tmp + '/e2e/'))
            .pipe(reporter({
                dest: reportLocation
            }));

      /*  var report_options = {
            theme: 'bootstrap',
            jsonFile: options.tmp +/e2e/+ 'cucumber-test-results.json',
            output: 'reports/report.html',
            //reportSuiteAsScenarios: true,
            launchReport: false
            /!*metadata: {
                "App Version":"0.3.2",
                "Test Environment": "STAGING",
                "Browser": "Chrome  54.0.2840.98",
                "Platform": "Windows 10",
                "Parallel": "Scenarios",
                "Executed": "Remote"
            }*!/
        };

        reporter.generate(report_options);*/

    });

    gulp.task('protractor', ['installTypings','transpile-e2e','webdriver-update'],runProtractor);


}